
create database dbOrder;
use dbOrder;
CREATE TABLE `Customer` (
	`Id` VARCHAR(50) NOT NULL ,
	`name` varchar(50) NOT NULL,
	`phone` VARCHAR(10) NOT NULL,
	`addres` varchar(100) NOT NULL,
	PRIMARY KEY (`Id`)
);

CREATE TABLE `Order` (
	`Id` VARCHAR(50) NOT NULL ,
	`orderDate` DATETIME NOT NULL,
	`deliveredDate` DATETIME NOT NULL,
	`shippingPrice` FLOAT NOT NULL,
	`goodsPrice` FLOAT NOT NULL,
	`shipper` varchar(50) NOT NULL,
	`shop` varchar(50) NOT NULL,
	`custId` VARCHAR(50) NOT NULL,
	`status` VARCHAR(20) NOT NULL,
	PRIMARY KEY (`Id`)
);

CREATE TABLE `Order_Product` (
	`orderId` VARCHAR(50) NOT NULL,
	`name` varchar(50) NOT NULL,
	`price` FLOAT NOT NULL,
	`quantity` INT NOT NULL
);

CREATE TABLE `Payment` (
	`Id` VARCHAR(50) NOT NULL ,
	`shippingCost` FLOAT NOT NULL,
	`goodsCost` FLOAT NOT NULL,
	`orderId` VARCHAR(50) NOT NULL,
	PRIMARY KEY (`Id`)
);

ALTER TABLE `Order` ADD CONSTRAINT `Order_fk0` FOREIGN KEY (`custId`) REFERENCES `Customer`(`Id`);

ALTER TABLE `Order_Product` ADD CONSTRAINT `Order_Product_fk0` FOREIGN KEY (`orderId`) REFERENCES `Order`(`Id`);

ALTER TABLE `Payment` ADD CONSTRAINT `Payment_fk0` FOREIGN KEY (`orderId`) REFERENCES `Order`(`Id`);
