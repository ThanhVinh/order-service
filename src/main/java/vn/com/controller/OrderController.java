package vn.com.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1")
public class OrderController {
	@GetMapping("/testApi")
	public ResponseEntity<?> testApi(){
		return ResponseEntity.ok("test success");
	}
}
