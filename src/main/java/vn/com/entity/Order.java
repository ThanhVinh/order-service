package vn.com.entity;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "order")
public class Order {
	@Id
	@GeneratedValue(generator = "uuid2")
	@GenericGenerator(name = "uuid2", strategy = "uuid2")
	private String id;
	
	@ManyToOne
	@JoinColumn(name = "cust_id")
	private Customer customer;
	
	@Column(name = "order_date")
	private LocalDate orderDate;
	
	@Column(name = "delivered_date")
	private LocalDate deliveredDate;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "shipping_price")
	private Float shippingPrice;
	
	@Column(name = "goods_price")
	private Float goodsPrice;
	
	@Column(name = "shipper")
	private String shipper;
	
	@Column(name = "shop")
	private String shop;
	
	@OneToMany(mappedBy = "order")
	private List<OrderProduct> orderProducts;
	
	@OneToMany(mappedBy = "order")
	private List<Payment> payments;
	
}
