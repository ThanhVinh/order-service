package vn.com.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor 
@AllArgsConstructor
public class PaymentDto {
	private OrderDto order;
	private String orderId;
	private Float shippingCost;
	private Float goodsCost;
}
