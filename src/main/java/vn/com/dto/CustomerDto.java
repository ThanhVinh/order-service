package vn.com.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor 
@AllArgsConstructor 
public class CustomerDto {
	private String name;
	private String address;
	private String phone;
	private List<OrderDto> orders;
}
