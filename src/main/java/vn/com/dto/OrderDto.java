package vn.com.dto;

import java.time.LocalDate;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import vn.com.entity.Customer;

@Data
@SuperBuilder
@NoArgsConstructor 
@AllArgsConstructor
public class OrderDto {
	private LocalDate orderDate;
	private Customer customer;
	private String custId;
	private LocalDate deliveredDate;
	private String status;
	private Float shippingPrice;
	private Float goodsPrice;
	private String shipper;
	private String shop;
	private List<OrderProductDto> orderProducts;
}
