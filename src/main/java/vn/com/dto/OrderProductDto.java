package vn.com.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor 
@AllArgsConstructor
public class OrderProductDto {
	private OrderDto order;
	private String orderId;
	private String name;
	private Float price;
	private int quantity;
	private String productCode;
}
