package vn.com.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;
import org.mapstruct.ReportingPolicy;

import vn.com.dto.OrderProductDto;
import vn.com.entity.OrderProduct;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
@Named("OrderProductMapper")
public interface OrderProductMapper {
	@Named("toDtoWithoutChildren")
    @Mappings({
            @Mapping(target = "order", qualifiedByName = {"OrderMapper", "toDtoWithoutChildren"})})
	OrderProductDto entityToDto (OrderProduct orderProduct);
	OrderProduct dtoToEntity (OrderProductDto orderProductDto);
}
