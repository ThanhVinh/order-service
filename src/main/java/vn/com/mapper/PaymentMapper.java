package vn.com.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Named;
import org.mapstruct.ReportingPolicy;

import vn.com.dto.PaymentDto;
import vn.com.entity.Payment;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
@Named("PaymentMapper")
public interface PaymentMapper {
	PaymentDto entityToDto (Payment payment);
	Payment dtotoEntity (PaymentDto paymentDto);
}
