package vn.com.mapper;

import org.mapstruct.*;
import vn.com.dto.OrderDto;
import vn.com.entity.Order;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
@Named("OrderMapper")
public interface OrderMapper {
	@Named("toDtoWithoutChildren")
    @Mappings({
            @Mapping(target = "customer", qualifiedByName = {"CustomerMapper", "toDtoWithoutOrder"})})
	OrderDto entityToDto (Order order);
	Order dtoToEntity (OrderDto orderDto);
}
