package vn.com.mapper;

import org.mapstruct.*;

import vn.com.dto.CustomerDto;
import vn.com.entity.Customer;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
@Named("CustomerMapper")
public interface CustomerMapper {
	@Named("toDtoWithoutOrder")
    @Mappings({ @Mapping(target = "orders", expression = "java(null)")})
	CustomerDto entityToDto (Customer customer);
	Customer dtoToEntity (CustomerDto customerDto);
}
